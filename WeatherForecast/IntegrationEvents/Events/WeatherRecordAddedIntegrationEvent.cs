﻿using WeatherForecast.EventBus.Events;

namespace WeatherForecast.IntegrationEvents.Events
{
    public record WeatherRecordAddedIntegrationEvent : IntegrationEvent
    {
        public string City { get; }
        public WeatherRecordAddedIntegrationEvent(string city) => City = city;
    }
}
