﻿using WeatherForecast.EventBus.Events;

namespace WeatherForecast.IntegrationEvents.Events
{
    public record WeatherForecastRequestedIntegrationEvent : IntegrationEvent
    {
    }
}
