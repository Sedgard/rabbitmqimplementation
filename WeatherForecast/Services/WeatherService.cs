﻿using System;
using System.Collections.Generic;
using System.Linq;
using WeatherForecast.Controllers;
using WeatherForecast.EventBus.Abstractions;
using WeatherForecast.IntegrationEvents.Events;

namespace WeatherForecast.Services
{
    public class WeatherService
    {
        private readonly IDictionary<string, IList<WeatherForecast>> forecasts = new Dictionary<string, IList<WeatherForecast>>();
        private static readonly string[] Summaries = new[]
{
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };
        private readonly IEventBus _eventBus;
        public WeatherService(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }
        public WeatherForecast AddForecast(WeatherRecordModel forecast)
        {
            var rng = new Random();
            if (!forecasts.ContainsKey(forecast.City))
                forecasts[forecast.City] = new List<WeatherForecast>();
            var newForecast = new WeatherForecast()
            {
                Date = forecast.Date,
                TemperatureC = forecast.TemperatureC,
                Summary = Summaries[rng.Next(Summaries.Length)]
            };
            forecasts[forecast.City].Add(newForecast);

            _eventBus.Publish(new WeatherRecordAddedIntegrationEvent(forecast.City));

            return newForecast;
        }

        public IEnumerable<WeatherForecast> ListForecasts()
        {

            _eventBus.Publish(new WeatherForecastRequestedIntegrationEvent());

            return forecasts.SelectMany(x => x.Value);
        }
    }
}
