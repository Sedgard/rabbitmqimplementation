﻿using System;

namespace WeatherForecast.Controllers
{
    public record WeatherRecordModel
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }
        public string City { get; set; }
    }
}
