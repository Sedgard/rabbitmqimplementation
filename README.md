# Sample implementation of RabbitMq
Inspired by [eShopOnContainer-EventBus](https://github.com/dotnet-architecture/eShopOnContainers/tree/dev/src/BuildingBlocks/EventBus)

### Simplified way to register at startup
```csharp
public static IServiceCollection RegisterEventBus(this IServiceCollection services, IConfiguration configuration)
{
    return services.AddRabbitMq(configuration)
        .AddIntegratedEventHandler<WeatherForecastRequestedIntegrationEventHandler>()
        .AddIntegratedEventHandler<WeatherRecordAddedIntegrationEventHandler>();
}
public static IApplicationBuilder ConfigureEventBus(this IApplicationBuilder app, IEventBus eventBus)
{
    eventBus.Subscribe<WeatherForecastRequestedIntegrationEvent, WeatherForecastRequestedIntegrationEventHandler>();
    eventBus.Subscribe<WeatherRecordAddedIntegrationEvent, WeatherRecordAddedIntegrationEventHandler>();
    return app;
}
```

### Unified RabbitMq config in appsettings
```json
"RabbitMQ": {
    "RetryCount": 5,
    "SubScriptionClientName": "WeatherForecastWatch",
    "UserName": "guest",
    "Password": "guest",
    "Connection": "localhost"
}
```

### Removed dependency on Autofac
Still looking on the difference between
`_scopeFactory.CreateScope()` and `_autofac.BeginLifetimeScope(AUTOFAC_SCOPE_NAME)`
