﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MQTTnet;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;
using System;
using System.Threading.Tasks;

namespace WeatherForecast.EventBusMQTT
{
    public class DefaultMQTTPersistentConnection
        : IMQTTPersistentConnection
    {
        private readonly ILogger<DefaultMQTTPersistentConnection> _logger;
        private readonly IManagedMqttClient _managedMqttClient;
        private ManagedMqttClientOptions _mqttOptions;
        bool _disposed;

        public DefaultMQTTPersistentConnection(ILogger<DefaultMQTTPersistentConnection> logger, IOptions<MQTTConfig> mqttOptions)
        {
            _logger = logger;
            var config = mqttOptions.Value;

            _mqttOptions = new ManagedMqttClientOptionsBuilder()
            .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
            .WithClientOptions(new MqttClientOptionsBuilder()
                .WithClientId(config.SubScriptionClientName)
                .WithTcpServer(config.Connection)
                .WithCredentials(config.UserName, config.Password)
                .Build())
            .Build();

            _managedMqttClient = new MqttFactory().CreateManagedMqttClient();
            _managedMqttClient.UseDisconnectedHandler(HandleDisconnect);
            _managedMqttClient.UseConnectedHandler(HandleConnect);

        }

        public bool IsConnected => throw new NotImplementedException();

        public bool TryConnect()
        {
            throw new NotImplementedException();
        }

        public IManagedMqttClient MQTTClient()
        {
            if(!_managedMqttClient.IsStarted)
                _managedMqttClient.StartAsync(_mqttOptions);

            return _managedMqttClient;
        }

        public void Dispose()
        {

            if (_disposed) return;

            _disposed = true;

            try
            {
                _managedMqttClient.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex.ToString());
            }
        }

        private void HandleDisconnect(MqttClientDisconnectedEventArgs e)
        {
            _logger.LogWarning("mqqt client has disconnected");
        }

        private void HandleConnect(MqttClientConnectedEventArgs arg)
        {
            _logger.LogWarning("mqqt client has disconnected");
        }
    }
}
