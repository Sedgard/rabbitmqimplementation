﻿using MQTTnet.Extensions.ManagedClient;
using System;

namespace WeatherForecast.EventBusMQTT
{
    public interface IMQTTPersistentConnection
        : IDisposable
    {
        //bool IsConnected { get; }

        //bool TryConnect();
        IManagedMqttClient MQTTClient();
        //IManagedMqttClient CreateModel();
    }
}
