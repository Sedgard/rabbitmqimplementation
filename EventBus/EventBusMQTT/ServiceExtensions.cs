﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WeatherForecast.EventBus;
using WeatherForecast.EventBus.Abstractions;

namespace WeatherForecast.EventBusMQTT
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddMQTTClient(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<MQTTConfig>(configuration.GetSection("MQTTClient"));

            services.AddSingleton<IMQTTPersistentConnection, DefaultRabbitMQPersistentConnection>();

            services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

            services.AddSingleton<IEventBus, EventBusMQTT>();


            return services;
        }
        public static IServiceCollection AddIntegratedEventHandler<T>(this IServiceCollection services)
            where T : IIntegrationEventHandler
        {
            services.AddTransient(typeof(T));

            return services;
        }
    }
}
