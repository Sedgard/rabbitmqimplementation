﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WeatherForecast.EventBus;
using WeatherForecast.EventBus.Abstractions;

namespace WeatherForecast.EventBusRabbitMQ
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddRabbitMq(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<RabbitMQConfig>(configuration.GetSection("RabbitMQ"));

            services.AddSingleton<IRabbitMQPersistentConnection, DefaultRabbitMQPersistentConnection>();

            services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

            services.AddSingleton<IEventBus, EventBusRabbitMQ>();


            return services;
        }
        public static IServiceCollection AddIntegratedEventHandler<T>(this IServiceCollection services)
            where T : IIntegrationEventHandler
        {
            services.AddTransient(typeof(T));

            return services;
        }
    }
}
