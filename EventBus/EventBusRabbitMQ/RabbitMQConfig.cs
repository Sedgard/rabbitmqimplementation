﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherForecast.EventBusRabbitMQ
{
    public record RabbitMQConfig
    {
        public int RetryCount { get; set; } = 5;
        public string SubScriptionClientName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Connection { get; set; }
    }
}
