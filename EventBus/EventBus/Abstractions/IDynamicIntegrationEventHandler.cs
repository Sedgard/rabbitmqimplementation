﻿using System.Threading.Tasks;

namespace WeatherForecast.EventBus.Abstractions
{
    public interface IDynamicIntegrationEventHandler
    {
        Task Handle(dynamic eventData);
    }
}
