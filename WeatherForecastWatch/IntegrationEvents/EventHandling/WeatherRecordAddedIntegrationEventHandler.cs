﻿using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using WeatherForecast.EventBus.Abstractions;
using WeatherForecastWatch.IntegrationEvents.Events;
using WeatherForecastWatch.Services;

namespace WeatherForecastWatch.IntegrationEvents.EventHandling
{
    public class WeatherRecordAddedIntegrationEventHandler :
        IIntegrationEventHandler<WeatherRecordAddedIntegrationEvent>
    {
        private readonly ILogger<WeatherRecordAddedIntegrationEventHandler> _logger;
        private readonly WeatherStatsService _weatherStatsService;
        public WeatherRecordAddedIntegrationEventHandler(
            ILogger<WeatherRecordAddedIntegrationEventHandler> logger,
            WeatherStatsService weatherStatsService)
        {
            _logger = logger;
            _weatherStatsService = weatherStatsService;
        }
        public Task Handle(WeatherRecordAddedIntegrationEvent @event)
        {
            _weatherStatsService.WeatherRecordAdded(@event.City);
            return Task.CompletedTask;
        }
    }
}
