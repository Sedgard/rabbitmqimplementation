﻿using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using WeatherForecast.EventBus.Abstractions;
using WeatherForecastWatch.IntegrationEvents.Events;
using WeatherForecastWatch.Services;

namespace WeatherForecastWatch.IntegrationEvents.EventHandling
{
    public class WeatherForecastRequestedIntegrationEventHandler :
        IIntegrationEventHandler<WeatherForecastRequestedIntegrationEvent>
    {
        private readonly ILogger<WeatherForecastRequestedIntegrationEventHandler> _logger;
        private readonly WeatherStatsService _weatherStatsService;
        public WeatherForecastRequestedIntegrationEventHandler(
            ILogger<WeatherForecastRequestedIntegrationEventHandler> logger,
            WeatherStatsService weatherStatsService)
        {
            _logger = logger;
            _weatherStatsService = weatherStatsService;
        }
        public Task Handle(WeatherForecastRequestedIntegrationEvent @event)
        {
            _weatherStatsService.WeatherForecastRetrieved();
            return Task.CompletedTask;
        }
    }
}
