﻿using WeatherForecast.EventBus.Events;

namespace WeatherForecastWatch.IntegrationEvents.Events
{
    public record WeatherForecastRequestedIntegrationEvent : IntegrationEvent
    {
    }
}
