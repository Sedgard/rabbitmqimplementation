﻿using WeatherForecast.EventBus.Events;

namespace WeatherForecastWatch.IntegrationEvents.Events
{
    public record WeatherRecordAddedIntegrationEvent : IntegrationEvent
    {
        public string City { get; }
        public WeatherRecordAddedIntegrationEvent(string city) => City = city;
    }
}
