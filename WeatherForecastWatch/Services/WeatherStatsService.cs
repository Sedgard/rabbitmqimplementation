﻿using System.Collections.Generic;

namespace WeatherForecastWatch.Services
{
    public class WeatherStatsService
    {
        private readonly IDictionary<string, int> _weatherRecords = new Dictionary<string, int>();
        private int _weatherForecastAnswer;

        public void WeatherRecordAdded(string city)
        {
            if (!_weatherRecords.ContainsKey(city))
                _weatherRecords.Add(city, 1);
            else
                _weatherRecords[city]++;
        }

        public void WeatherForecastRetrieved()
        {
            _weatherForecastAnswer++;
        }

        public (IDictionary<string, int>, int) WeatherStats()
        {
            return (_weatherRecords, _weatherForecastAnswer);
        }
    }
}
