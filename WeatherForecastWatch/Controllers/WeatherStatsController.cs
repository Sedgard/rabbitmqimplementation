﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherForecastWatch.Services;

namespace WeatherForecastWatch.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherStatsController : ControllerBase
    {

        private readonly ILogger<WeatherStatsController> _logger;
        private readonly WeatherStatsService _weatherCountService;

        public WeatherStatsController(ILogger<WeatherStatsController> logger,
            WeatherStatsService weatherCountService)
        {
            _logger = logger;
            _weatherCountService = weatherCountService;
        }

        [HttpGet]
        public WeatherForecastStats Get()
        {
            var stats = _weatherCountService.WeatherStats();
            return new WeatherForecastStats()
            {
                WeatherRecords = stats.Item1,
                WeatherForecastAnswer = stats.Item2
            };
        }
    }
}
