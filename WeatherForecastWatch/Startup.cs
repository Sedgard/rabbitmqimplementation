using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using WeatherForecast.EventBus.Abstractions;
using WeatherForecastWatch.Services;
using WeatherForecast.EventBusRabbitMQ;
using WeatherForecastWatch.IntegrationEvents.EventHandling;
using WeatherForecastWatch.IntegrationEvents.Events;

namespace WeatherForecastWatch
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<WeatherStatsService>();
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WeatherForecastWatch", Version = "v1" });
            });
            services.RegisterEventBus(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IEventBus eventBus)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WeatherForecastWatch v1"));
            }

            app.ConfigureEventBus(eventBus);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }


    }
    public static class ServiceExtensions
    {
        public static IServiceCollection RegisterEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddRabbitMq(configuration)
                .AddIntegratedEventHandler<WeatherForecastRequestedIntegrationEventHandler>()
                .AddIntegratedEventHandler<WeatherRecordAddedIntegrationEventHandler>();
        }
        public static IApplicationBuilder ConfigureEventBus(this IApplicationBuilder app, IEventBus eventBus)
        {
            eventBus.Subscribe<WeatherForecastRequestedIntegrationEvent, WeatherForecastRequestedIntegrationEventHandler>();
            eventBus.Subscribe<WeatherRecordAddedIntegrationEvent, WeatherRecordAddedIntegrationEventHandler>();
            return app;
        }
    }
}
