using System.Collections.Generic;

namespace WeatherForecastWatch
{
    public record WeatherForecastStats
    {
        public IDictionary<string, int> WeatherRecords { get; set; }
        public int WeatherForecastAnswer { get; set; }
    }
}
